"""
ref. https://falcon.readthedocs.io/en/stable/user/quickstart.html
"""

import falcon
from wsgiref.simple_server import make_server
#
import os

class IndexResource:
    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200
        resp.json   = {}


app = falcon.App(
    cors_enable=eval( os.environ.get('CORS_ENABLE', '1') ),  # True to allow CORS ref. https://falcon.readthedocs.io/en/stable/api/cors.html#cors
)

app.add_route('/', IndexResource())


if __name__ == '__main__':
    port = int( os.environ.get('PORT', 8000) )
    with make_server(host='0.0.0.0', port=port, app=app) as httpd:
        print(f'Serving on port {port}...')
        httpd.serve_forever()
