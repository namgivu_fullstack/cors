SH=`cd ${BASH_SOURCE%/*} && pwd`
AH=`cd "$SH/../.."  && pwd` ; [ -z $BASH_SOURCE ] && AH='/app'  # hardcoded set when BASH_SOURCE not available - see why https://gitlab.com/namgivu_fullstack/linux_shell_script/-/commit/123ce39f5f1028310625bd133908f96f404bd374#70b15990581b1f994cb50ee4d858d80551cbd34f_0_44

cd $AH
    PYTHONPATH=$AH \
        python3 -m pipenv install

    PYTHONPATH=$AH \
        python3 -m pipenv run \
            python "$AH/falcon_apiapp/app.py"
