set -e  # halt if error

p=${PORT:-21201}
i=namgivu_frontend__cors__s2020apiappfalcon_i ; docker image rm -f $i
c=${i}__c${p} ; docker rm -f $c

SH=`cd ${BASH_SOURCE%/*} && pwd` ; [ -z $SH ] && SH='/app'  # handle SH for both bash+sh shell ref. https://gitlab.com/namgivu_fullstack/linux_shell_script/-/commit/123ce39f5f1028310625bd133908f96f404bd374#70b15990581b1f994cb50ee4d858d80551cbd34f_0_44
AH=`cd "$SH/.."          && pwd`

[ ! -z $nocache ] && nocache='--no-cache'
docker build $nocache \
    -t $i \
    -f "$AH/zcontainer/Dockerfile" \
    $AH

docker run -d \
    --name $c \
    -p $p:8000 \
    --env CORS_ENABLE=${CORS_ENABLE:-1} \
    $i

echo; docker ps -a | grep -E "$i|IMAGE|$c|NAME" --color=always

echo; cat <<EOT
--- aftermath command ON
== view run log
watch docker logs $c
docker logs $c
== request healthcheck
curl -I -X GET http://127.0.0.1:$p
http :$p

== nn frequentused
docker logs $c
http :$p
--- aftermath command OFF
EOT
