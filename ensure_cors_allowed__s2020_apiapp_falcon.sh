PORT_CORS_ENABLE0=20000
PORT_CORS_ENABLE1=21111

# up container
SH=`cd ${BASH_SOURCE%/*} && pwd`
PORT=$PORT_CORS_ENABLE0 CORS_ENABLE=0 "$SH/sample_app/s2020_apiapp_falcon/zcontainer/docker_buildrun.sh"
PORT=$PORT_CORS_ENABLE1 CORS_ENABLE=1 "$SH/sample_app/s2020_apiapp_falcon/zcontainer/docker_buildrun.sh"
sleep 3

echo;echo;echo;

# must FAIL as cors DISABLED
u="http://127.0.0.1:$PORT_CORS_ENABLE0"
curl -H "origin:http://anything"  -I -X GET $u -s | grep access-control-allow-origin -n6 --color=always ; [ "$?" -ne "0" ] && echo 'ERROR Test CORS-allowed failed' ;echo;echo
http $u "origin:http://anything"  --print=h       | grep access-control-allow-origin -n6 --color=always ; [ "$?" -ne "0" ] && echo 'ERROR Test CORS-allowed failed' ;echo;echo

echo;echo;echo;

# must PASS as cors ENABLED
u="http://127.0.0.1:$PORT_CORS_ENABLE1"
curl -H "origin:http://anything"  -I -X GET $u -s | grep access-control-allow-origin -n6 --color=always ; [ "$?" -ne "0" ] && echo 'ERROR Test CORS-allowed failed' ;echo;echo
http $u "origin:http://anything"  --print=h       | grep access-control-allow-origin -n6 --color=always ; [ "$?" -ne "0" ] && echo 'ERROR Test CORS-allowed failed' ;echo;echo
