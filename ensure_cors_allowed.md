ref. https://stackoverflow.com/a/47609921/248616
> If the response includes `access-control-allow-*` then your resource supports CORS.

ref. https://docs.aws.amazon.com/apigateway/latest/developerguide/apigateway-test-cors.html
> The headers in the response 
>   Access-Control-Allow-Origin 
>   Access-Control-Allow-Headers 
>   Access-Control-Allow-Methods  
> show that the API supports CORS


```
curl            -H "origin:http://anything"  -I -X GET $YOUR_API_URL
http $YOUR_API_URL "origin:http://anything" 

sample_console_output='
HTTP/1.0 200 OK
Date: Wed, 30 Nov 2022 07:33:05 GMT
Server: WSGIServer/0.2 CPython/3.8.10
access-control-allow-origin: *
content-length: 2
content-type: application/json
```
